jQuery(document).ready(function(){
  var hide_old_COs = {
    13: 'root (Request Tracker)', // not an old CO but not someone we foresee wanting to set Ownership to
    31: 'crc10 (Chris Chalk)',
    567395: 'cwc50 (Charles Callow)',
    369285: 'lc413 (Lisa Chapman)',
    2477: 'rc454 (Russell Currie)',
    662255: 'rmh73 (Robert Harrison)',
    798936: 'rww31 (Reece Waller)',
    333261: 'sb999 (Sebastian Brazell)',
    399295: 'sgc41 (Sabina Cole)',
    738188: 'tgm30 (Tanaka Mutsatsa)',
    882654: 'cdc45 (Christopher Chiocca)',
    824666: 'ajh221 (Adam Hall)',
    939543: 'tdw47 (Thomas Walters)',
  };

  var owner = jQuery('#Owner');
  if (owner) {
    jQuery.each(hide_old_COs, function(val, person) {
      var potential_old_co = jQuery('option[value="' + val + '"]', owner);
      if ((potential_old_co.text().trim() == person) && !potential_old_co.prop('selected')) {
        potential_old_co.detach();
      }
    });
  }
});
