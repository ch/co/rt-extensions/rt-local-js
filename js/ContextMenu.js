jQuery(document).ready(function(){

jQuery('body').removeAttr('contextmenu');
jQuery('menu#chemrtmenu').remove();
var smenu=jQuery('<menu>').attr('label','RT-Action');
jQuery('li#li-page-actions ul li a').each(function(i,e){

  smenu.append(
    jQuery('<menuitem>')
    .attr('label',jQuery(this).text())
    .attr('href',jQuery(this).attr('href'))
    .click(
      function(){
        window.open(jQuery(this).attr('href'),'_self');
      }
    )
  );
});
var cmenu=jQuery('<menu>');
cmenu.attr('id','chemrtmenu');
cmenu.attr('type','context');
cmenu.append(smenu);
jQuery('body').append(cmenu);
jQuery('body').attr('contextmenu','chemrtmenu');

jQuery('table.ticket-list tr menu').remove();
jQuery('table.ticket-list tr').each(function(){
  tid=jQuery(this).find('td').first().text();
  if (tid) {
    jQuery(this).attr('ticket',tid);
    jQuery(this).find('td').first().append(
      jQuery('<menu>').attr('id','ticket'+tid).attr('type','context').append(
       jQuery('<menu>').attr('label','RT-Action').append(
        jQuery('<menuitem>').attr('label','Quick Open').click(function(i,e){
          tid=jQuery(this).closest('tr[ticket]').attr('ticket');
          window.open('/rt/Ticket/Display.html?Status=open&id='+tid,'_self');
        })
       )
      )
    );
    jQuery(this).attr('contextmenu','ticket'+tid);
    jQuery(this).next().attr('contextmenu','ticket'+tid);
  }
});
});
